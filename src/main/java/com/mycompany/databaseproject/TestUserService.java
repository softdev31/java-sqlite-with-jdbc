/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject;

import com.mycompany.databaseproject.model.User;
import com.mycompany.databaseproject.service.UserService;

/**
 *
 * @author ASUS
 */
public class TestUserService {
    public static void main(String[] args) {
        UserService usersevice = new UserService();
        User user = usersevice.login("admina", "1234");
        if(user!=null){
            System.out.println("Welcom user : "+ user.getName());
        }else{
            System.out.println("Error");
        }
    }
}
